## Git global setup
```
git config --global user.name "Simone Francescato"
git config --global user.email "simone.francescato@cern.ch"
```

## Clone a new repository locally and commit a change
```
git clone https://:@gitlab.cern.ch:8443/sfrances/git-mini-tutorial.git
cd git-mini-tutorial
touch file.txt
git add file.txt
git commit -m "message"
git push origin master
```
